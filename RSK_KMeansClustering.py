#!/usr/bin/env python
# coding: utf-8

# # RSK_KMeansClustering_CustomerSegmentation_RFM
# #By- Aarush Kumar
# #Dated: August 26,2021

# In[1]:


import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt


# In[6]:


df = pd.read_csv("/home/aarush100616/Downloads/Projects/Ecommerce Analysis/data.csv",encoding= 'unicode_escape')
df


# In[7]:


df.info()


# In[8]:


import re
df['InvoiceNo'].str.contains('C', flags = re.IGNORECASE, regex = True)


# In[9]:


df[df['InvoiceNo'].str.contains('C', flags = re.IGNORECASE, regex = True)]


# In[10]:


idx_cancelled_invoices = df[df['InvoiceNo'].str.contains('C', flags = re.IGNORECASE, regex = True)].index
idx_cancelled_invoices


# In[11]:


df = df[~(df['InvoiceNo'].str.contains('C', flags = re.IGNORECASE, regex = True))]
df


# In[12]:


df.isnull().sum()


# In[13]:


print(f"Before Dropping CustomerID Null Rows:\nNumber of rows = {df.shape[0]}\nNumber of cols = {df.shape[1]}")
df.dropna(inplace = True)
print(f"\nAfter Dropping CustomerID Null Rows:\nNumber of rows = {df.shape[0]}\nNumber of cols = {df.shape[1]}")


# In[14]:


df.isnull().sum()


# In[15]:


print(f"Before Dropping Duplicates:\nNumber of rows = {df.shape[0]}\nNumber of cols = {df.shape[1]}")
df.drop_duplicates(inplace = True)
print(f"\nAfter Dropping Duplicates:\nNumber of rows = {df.shape[0]}\nNumber of cols = {df.shape[1]}")


# In[16]:


df.head(10)


# In[17]:


df['CustomerID'] = df['CustomerID'].astype('int64').astype('category')
df['CustomerID'].dtype


# In[18]:


df['InvoiceDate'] = pd.to_datetime(df['InvoiceDate'])
df['InvoiceDate'].dtype


# In[19]:


df.describe()


# In[20]:


df['TotalPurchaseValue'] = df['Quantity'] * df['UnitPrice']
df.head(10)


# In[21]:


customer_wise_total_purchase_value_df = df[['CustomerID', 'TotalPurchaseValue']].groupby('CustomerID', as_index = False).sum()
customer_wise_total_purchase_value_df.rename(columns = {'TotalPurchaseValue' : 'Monetary'}, inplace = True)
customer_wise_total_purchase_value_df


# In[22]:


customer_wise_frequent_purchases_df =  df[['CustomerID', 'InvoiceNo']].groupby('CustomerID', as_index = False).count()
customer_wise_frequent_purchases_df.rename(columns = {'InvoiceNo': 'Frequency'}, inplace = True)
customer_wise_frequent_purchases_df


# In[23]:


merged_df = customer_wise_total_purchase_value_df.merge(customer_wise_frequent_purchases_df, on = "CustomerID", how = "inner")
merged_df


# In[24]:


last_purchase_df = df[['CustomerID', 'InvoiceDate']].groupby('CustomerID', as_index = False).max()
last_purchase_df.rename(columns = {'InvoiceDate': 'LastPurchaseDate'}, inplace = True)
last_purchase_df


# In[25]:


days_since_last_purchase = df['InvoiceDate'].max() - last_purchase_df['LastPurchaseDate'] 
days_since_last_purchase = days_since_last_purchase + pd.Timedelta("1 days")
days_since_last_purchase


# In[26]:


time_diff_in_days = pd.Series(data = [d.days for d in days_since_last_purchase], index = merged_df.index)
time_diff_in_days


# In[27]:


merged_df['Recency'] = time_diff_in_days
merged_df


# In[28]:


merged_df.isnull().sum()


# In[29]:


merged_df.dropna(inplace = True)
merged_df.drop(columns = 'CustomerID', inplace = True)


# In[30]:


merged_df.isna().sum()


# In[31]:


fig, axis = plt.subplots(nrows = 2, ncols = 3, 
                         figsize = (15, 4), dpi = 100,
                         sharex = False, sharey = False,
                         gridspec_kw = {'height_ratios': [3, 1]}
                         )

# Monetary
axis[0, 0].hist(merged_df['Monetary'], bins = 'sturges', facecolor = 'red', edgecolor = 'black')
sns.boxplot(x = 'Monetary', data = merged_df,  color = 'red', ax = axis[1, 0])
axis[0, 0].set_title("Histogram & Boxplot for Monetary")

# Frequency
axis[0, 1].hist(merged_df['Frequency'], bins = 'sturges', facecolor = 'green', edgecolor = 'black')
sns.boxplot(x = 'Frequency', data = merged_df,  color = 'green', ax = axis[1, 1])
axis[0, 1].set_title("Histogram & Boxplot for Frequency")

# Recency
axis[0, 2].hist(merged_df['Recency'], bins = 'sturges', facecolor = 'purple', edgecolor = 'black')
sns.boxplot(x = 'Recency', data = merged_df,  color = 'purple', ax = axis[1, 2])
axis[0, 2].set_title("Histogram & Boxplot for Recency")

plt.show()


# In[32]:


def treating_outliers(df, col):
  col_q1 = df[col].quantile(0.25)
  col_q3 = df[col].quantile(0.75)
  col_iqr = col_q3 - col_q1
  new_df = df[(df[col] >= col_q1 - 1.5 * col_iqr) & (df[col] <= col_q3 + 1.5 * col_iqr)]
  return new_df

new_df = treating_outliers(merged_df, 'Monetary')
new_df = new_df.reset_index(drop = True)
new_df


# In[33]:


fig, axis_mon = plt.subplots(nrows = 2, ncols = 1, 
                         figsize = (10, 4), dpi = 100,
                         sharex = False, sharey = False,
                         gridspec_kw = {'height_ratios': [3, 1]}
                         )

# Monetary
axis_mon[0].hist(new_df['Monetary'], bins = 'sturges', facecolor = 'red', edgecolor = 'black')
sns.boxplot(x = 'Monetary', data = new_df,  color = 'red', ax = axis_mon[1])
axis_mon[0].set_title("Histogram & Boxplot for Monetary")

plt.show()


# In[34]:


# standardise all parameters
from sklearn.preprocessing import StandardScaler

standard_scaler = StandardScaler()
norm_new = standard_scaler.fit_transform(new_df)
norm_new_df = pd.DataFrame(norm_new)
norm_new_df.columns = new_df.columns
norm_new_df


# In[35]:


norm_new_df.describe().loc[['mean', 'std'], :]


# In[36]:


from sklearn.neighbors import NearestNeighbors
from random import sample
from numpy.random import uniform
import numpy as np
from math import isnan
 
def hopkins(X):
    d = X.shape[1]
    #d = len(vars) # columns
    n = len(X) # rows
    m = int(0.1 * n) 
    nbrs = NearestNeighbors(n_neighbors=1).fit(X.values)
 
    rand_X = sample(range(0, n, 1), m)
 
    ujd = []
    wjd = []
    for j in range(0, m):
        u_dist, _ = nbrs.kneighbors(uniform(np.amin(X,axis=0),np.amax(X,axis=0),d).reshape(1, -1), 2, return_distance=True)
        ujd.append(u_dist[0][1])
        w_dist, _ = nbrs.kneighbors(X.iloc[rand_X[j]].values.reshape(1, -1), 2, return_distance=True)
        wjd.append(w_dist[0][1])
 
    H = sum(ujd) / (sum(ujd) + sum(wjd))
    if isnan(H):
        print(ujd, wjd)
        H = 0
 
    return H

hopkins(norm_new_df)


# In[37]:


# K Means with K = 5: chosen randomly.
from sklearn.cluster import KMeans

kmeans_model = KMeans(n_clusters = 5, init = 'k-means++', random_state = 14)
kmeans_model.fit(norm_new_df)

cluster_labels = pd.Series(data = kmeans_model.labels_, index = norm_new_df.index)
cluster_labels.value_counts()


# In[38]:


# analysis of clusters formed
km_df = pd.concat([new_df, cluster_labels], axis = 1)
km_df.columns = list(new_df.columns) + ['ClusterLabel']
km_df


# In[39]:


import plotly.express as px
# df = px.data.iris()
plotly_fig = px.scatter_3d(km_df, x = 'Monetary', y = 'Frequency', z = 'Recency', color = 'ClusterLabel')
plotly_fig.show()


# In[40]:


from sklearn.metrics import silhouette_score
sse_ = []
for k in range(2, 15):
    kmeans = KMeans(n_clusters = k).fit(norm_new_df)
    sse_.append([k, silhouette_score(norm_new_df, kmeans.labels_)])
    
plt.plot(pd.DataFrame(sse_)[0], pd.DataFrame(sse_)[1])


# In[41]:


# Rebuilding KMeans model with 4 clusters.
kmeans_model2 = KMeans(n_clusters = 4, init = 'k-means++', random_state = 14)
kmeans_model2.fit(norm_new_df)

cluster_labels2 = pd.Series(data = kmeans_model2.labels_, index = norm_new_df.index)
cluster_labels2.value_counts()


# In[42]:


# analysis of clusters formed
km_df2 = pd.concat([new_df, cluster_labels2], axis = 1)
km_df2.columns = list(new_df.columns) + ['ClusterLabel']
km_df2


# In[43]:


import plotly.express as px
# df = px.data.iris()
plotly_fig2 = px.scatter_3d(km_df2, x = 'Monetary', y = 'Frequency', z = 'Recency', color = 'ClusterLabel')
plotly_fig2.show()

